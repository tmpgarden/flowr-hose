var kue = require('kue');
var express = require('express');
var bodyParser = require('body-parser');
const uuidv4 = require('uuid/v4')

var pid = process.pid;

var Hose = function(args) {
  if(args) {
    this.args = args;
    this.jobsProcessed = 0;
    this.jobsCreated = 0;
    this.init();
  } else {
    console.error("flowr-hose:","No config, exiting");
    process.exit(1)
  }
};

Hose.prototype.init = function() {
  var self = this;
  var app = express();
  app.use(bodyParser.urlencoded({ extended: false }))
  app.use(bodyParser.json())
  this.service = this.args.name;
  this.queue = kue.createQueue(this.args.kue);

  if(this.queue) {
    this.queue.process(this.service + ':ping', ping);
  }

  app.get('/', function (req, res) {
    res.json(self.args.flowr || self.args.name);
  });

  app.get('/ping', function (req, res) {
    ping(function(err, data){
      if(err) return res.json(err);
      return res.json(data);
    });
  });

  app.get('/status', function (req, res) {
    return res.status(200).send('OK. Jobs processed: '+ self.jobsProcessed +'. Jobs created: '+ self.jobsCreated);
  });

  if(this.args.ui){
    kue.app.listen(this.args.ui.port || 3000);
    if(this.args.ui.title){
      kue.app.set('title', this.args.ui.title);
    }
  }

  this.http = {};
  this.http.app = app;
  this.http.server = app.listen(this.args.port, function () {
    var host = self.http.server.address().address;
    var port = self.http.server.address().port;
    console.log('FlowR microservice '+ self.service +' running at http://%s:%s', host, port);
  });
};

Hose.prototype.config = function(args) {
  this.args = args;
};

Hose.prototype.process = function(job, n, cb) {
  var self = this;
  console.log("PROCESSSING /"+job);
  if( 'function' == typeof n ) {
    cb = n;
    n = 1;
  }

  this.queue.process(this.service+':'+job, n, function(job, done){
    cb(job, function(err, result) {
      self.jobsProcessed++;
      done(err, result);
    });
  });

  // generate new route
  this.http.app.post('/' + job, function(req, res) {
    cb({data:req.body, log: console.log}, function(err, output) {
      if(err) return res.status(500).json({
        success: false,
        error: err
      });
      self.jobsProcessed++;
      res.json({
        success: true,
        output: output
      });
    });
  });
};

Hose.prototype.create = function(name, args, options) {
  var self = this;
  if (!options) options = {}
  if (!args._fToken) args._fToken = uuidv4()
  var _jobResolve
  var _jobReject
  var jobPromise = new Promise(function (resolve, reject) {
    _jobResolve = resolve
    _jobReject = reject
  });
  return new Promise(function(resolve, reject) {
    var job = self.queue.create(name, args)
    if (options.remove) job.removeOnComplete(options.remove)
    if (options.ttl) job.ttl(options.ttl)
    job.save( function(err){
      if( err ) {
        console.error("job.save error: ", err);
        if (options.cb) cb(err, null);
        reject(err)
      } else {
        self.jobsCreated++;
        job.promise = jobPromise
        resolve(job)
      }
    }).on('complete', function(result){
      if (options.cb) cb(null, result);
      _jobResolve(result)
    }).on('error', function(err){
      if (options.cb) cb(err, null);
      _jobReject(err)
    });
  });
};

function ping(job, done) {
  if(!done) done = job;
  return done(null, {ping:"pong"});
}

module.exports = Hose;
